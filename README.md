# NES Reference

a reference document for the Nintendo Entertainment System internal workings

download the latest version of the pdf here https://gitlab.com/Henny022/nes-reference/-/jobs/artifacts/master/raw/nes.pdf?job=pdflatex

## help making this
If you want to help making this, feel free to submit Issues and feature requests on GitLab.
Or fork this and submit some merge requests on your own.

## Where I get my information from
I collected information on different websites.
I listed my greatest sources in the Resources chapter of the document
If you own one of these sites and think that I did not give you enough credit or that I did use stuff that I'm not allowed to use please message me. I try my best to check licenses and stuff, but some sites make it a bit harder.
If you think that I used stuff that you own and did not credit you, please message me too, I'll try to correct that.

## Reuse this information
You can reuse all this information however you want. I'll do proper license stuff later (reading all the different available options takes time)